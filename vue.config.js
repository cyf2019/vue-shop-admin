module.exports = {
  // 声明配置节点，值为function
  chainWebpack: config => {
    // 发布模式 判断当前所处的编译模式
    config.when(process.env.NODE_ENV === 'production', config => {
      // 得到默认的打包入口，并清除，设置打包入口为./src/main-prod.js
      config
        .entry('app')
        .clear()
        .add('./src/main-prod.js')
      // 设置external属性
      config.set('externals', {
        vue: 'Vue',
        'vue-router': 'VueRouter',
        axios: 'axios',
        lodash: '_',
        echarts: 'echarts',
        nprogress: 'NProgress',
        'vue-quill-editor': 'VueQuillEditor'
      })
      // 找到html插件，并修改该插件的固定配置项 args是HTML插件的相关参数
      config.plugin('html').tap(args => {
        // 追加自定义的属性字段isProd
        args[0].isProd = true
        return args
      })
    })

    // 开发模式
    config.when(process.env.NODE_ENV === 'development', config => {
      config
        .entry('app')
        .clear()
        .add('./src/main-dev.js')
      config.plugin('html').tap(args => {
        args[0].isProd = false
        return args
      })
    })
  }
}
